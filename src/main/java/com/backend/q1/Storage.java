package com.backend.q1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Storage {
	List<Map<String, String>> a;

    public Storage() {
        a = new ArrayList<>();
    }

    /**
     * Time Complexity = O(n)
     * Number of map items in Array, n
     * The array is iterated only once.
     * 
     * Space Complexity = O(n*m)
     * Number of map items in Array, n
     * Number of items in map, m
     * 
     * 
     * @param anotherArray
     * @return string representation of stored items
     */
    public String store(List<Map<String, String>> anotherArray) {
    	if(anotherArray == null || anotherArray.size() == 0)
    		return "";
    	List<Map<String, String>> syncedList = Collections.synchronizedList(anotherArray);
    	String storeString = "";
    	StringBuilder sb = new StringBuilder();
    	for(Map<String, String> map : syncedList) {
    		String items = mapToString(map);
    		sb.append(items);
    		sb.append("\n");
    	}
    	String s = sb.toString();
    	s = s.substring(0, s.length()-1);
        return s;
    }

    /**
     * Time Complexity = Approx. O(n*m)
     * Number of items splited into array, n
     * The array is iterated only once, but for each key value pair, 1 iteration is skipped
     * 
     * Space Complexity = O(n*m)
     * Number of map items in Array, n
     * Number of items in map, m
     * 
     * @param dataString
     * @return
     */
    public List<Map<String, String>> load(String dataString) {
    	if(dataString == null || dataString.length() == 0)
    		return null;
    	Pattern pattern = Pattern.compile("(?<=\\n)|(?=\\n)|[\\=\\;]+");
    	List<Map<String, String>> a = new ArrayList<>();
    	Map<String, String> tempMap = new HashMap<>();
    	String[] items = pattern.split(dataString);
    	int length = items.length;
    	for(int i=0; i<length ; i+=2) {
    		if(items[i].equals("\n")) {
    			a.add(tempMap);
    			tempMap = new HashMap<>();
    			i--;
    		} else {
    			tempMap.put(items[i], items[i+1]);
    		}
    	}
    	a.add(tempMap);
        return a;
    }
    
    private <K, V> String mapToString(Map<K ,V> map) {
    	return map.entrySet()
    			.stream()
    			.map(entry -> (entry.getKey() == map ? "(this Map)" : entry.getKey())
    			+ "="
    			+ (entry.getValue() == map ? "(this Map)" : entry.getValue()))
    			.collect(Collectors.joining(";"));
    }

}
