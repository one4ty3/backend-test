package com.backend.q2;

public class Node {
	private String name;
	private int weight;
	
	public Node(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public int getWeight() {
		return weight;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ Node: ");
		sb.append(name);
		sb.append(", Weight: ");
		sb.append(weight);
		sb.append(" ]");
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (! (o instanceof Node)) {
			return false;
		}
		
		Node that = (Node) o;
		return this.name.equals(that.getName());
	}
}
