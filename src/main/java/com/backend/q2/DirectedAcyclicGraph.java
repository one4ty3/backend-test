package com.backend.q2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class DirectedAcyclicGraph {
	static final int INF = Integer.MAX_VALUE;
	private int numVertices;
    private Map<Node, List<Node>> graph;

	public DirectedAcyclicGraph(int vertices) { 
    	numVertices = vertices;
    	graph = new HashMap<>(numVertices);
    } 
    
    public void addEdge(Node u, Node v) {
    	if (u.getWeight() < 1 || v.getWeight() < 1)
    		return;
    	if (graph.containsKey(u)) {
	        List<Node> adjNodeList = graph.get(u);
	        if (adjNodeList == null) {
	        	adjNodeList = new LinkedList<Node>();
	        }
	        
	        if (v != null) {
	        	adjNodeList.add(v);
	        }
	        graph.put(u,adjNodeList);
	        
    	} else {
    		//graph dont't contain key, new node
    		List<Node> adjNodeList = new LinkedList<Node>();
    		adjNodeList.add(v);
    		graph.put(u,adjNodeList);
    	}
    	
    	if(!graph.containsKey(v)) {
    		List<Node> adjNodeList = new LinkedList<Node>();
    		graph.put(v, adjNodeList);
    	}
    }
    
    void topologicalSortUtil(Node node, Map<Node, Boolean> visited, Stack<Node> stack) { 
    	// Mark the current node as visited. 
        visited.put(node, true);

        // Recur for all the vertices adjacent to this vertex 
        Iterator<Node> it = graph.get(node).iterator(); 
        while (it.hasNext()) { 
            Node adjNode = it.next(); 
            if (!visited.get(adjNode))
                topologicalSortUtil(adjNode, visited, stack);
        } 
        // Push current vertex to stack which stores result 
        stack.push(node); 
    }
    
    /**
     * Time Complexity = O(V+E)
     * Number of Vertices and Edges in the graph
     * Each vertices is visited once.
     * 
     * Space Complexity = O(V)
     * Number of vertices
     * Each vertex that is sorted will be put into stack.
     */
    public Stack<Node> topologicalSort(Node originNode) { 

    	Stack<Node> stack = new Stack<>(); 
  
        // Mark all the vertices as not visited 
        Map<Node, Boolean> visited = new HashMap<>();
        Set<Node> nodesKey = graph.keySet();
        for (Node node : nodesKey) 
            visited.put(node, false);
        
        //rearrange set to start with given origin node
        Set<Node> nodes = new LinkedHashSet<>();
        nodes.add(originNode);
        nodes.addAll(nodesKey);
        // Call the recursive helper function to store 
        // Topological Sort starting from all vertices 
        // one by one
        for(Node node : nodes)
            if(visited.get(node) == false) 
                topologicalSortUtil(node, visited, stack); 
        return stack;
    }
    
    /**
     * Time Complexity = O(V+E)
     * Number of Vertices and Edges in the graph
     * We visit the vertex's and its adjacent vertices once.
     * 
     * Space Complexity = O(V)
     * Number of vertices
     * Longest distance to each vertex is stored.
     */
    public int longestDistance(Node originNode) {
        Stack<Node> topoSorted = topologicalSort(originNode);
    	Map<Node, Integer> distances = new HashMap<>();
    	Set<Node> nodesKey = graph.keySet();

    	//set to smallest int distance
    	for(Node node : nodesKey) {
    		if(node.equals(originNode))
    			distances.put(node, node.getWeight());
    		else
    			distances.put(node, Integer.MIN_VALUE);
    	}
    	
    	while(!topoSorted.empty()) {
    		Node node = topoSorted.pop();
    		if(distances.get(node) != Integer.MIN_VALUE) {
    			List<Node> adjNodes = graph.get(node);
    			for(Node adjNode : adjNodes) {
    				if(distances.get(adjNode) < distances.get(node) + adjNode.getWeight()) {
    					int dist = distances.get(node) + adjNode.getWeight();
    					distances.put(adjNode, dist);
    				}
    			}
    		}
    	}
    	int longestDist = 0;
    	for(Map.Entry<Node, Integer> entry : distances.entrySet()) {
    		if (entry.getValue() > longestDist) {
    			longestDist = entry.getValue();
    		}
    	}
    	return longestDist;
    }
}
