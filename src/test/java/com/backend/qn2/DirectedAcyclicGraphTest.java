package com.backend.qn2;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.backend.q2.DirectedAcyclicGraph;
import com.backend.q2.Node;

public class DirectedAcyclicGraphTest {
	
	public static DirectedAcyclicGraph graph;
    public static DirectedAcyclicGraph graph2;
    
	public static Node originNode;
    public static Node originNode2;
	
	@BeforeClass
	public static void generateGraph() {
		// a -> b -> c
		// |_________^
		graph = new DirectedAcyclicGraph(3);
		Node aNode = new Node("A", 1);
		Node bNode = new Node("B", 2);
		Node cNode = new Node("C", 2);
		originNode = aNode;
		graph.addEdge(aNode, bNode);
		graph.addEdge(aNode, cNode);
		graph.addEdge(bNode, cNode);
	}
	
	@BeforeClass
    public static void generateGraph2() {
	    //        
	    //        
        // a -> b -> c
        //  |      |___> d       
	    //  |_______|
        graph2 = new DirectedAcyclicGraph(3);
        Node aNode = new Node("A", 1);
        Node bNode = new Node("B", 2);
        Node cNode = new Node("C", 1);
        Node dNode = new Node("D", 3);
        originNode2 = aNode;
        graph2.addEdge(aNode, bNode);
        graph2.addEdge(aNode, dNode);
        graph2.addEdge(bNode, cNode);
        graph2.addEdge(bNode, dNode);
    }
	
	@Test
	public void testlongestDistance() {
		int actualDist = graph.longestDistance(originNode);
		assertEquals(5, actualDist);
	}
	
	@Test
    public void testlongestDistance2() {
        int actualDist = graph2.longestDistance(originNode2);
        assertEquals(6, actualDist);
    }
}
