package com.backend.qn1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.backend.q1.Storage;

public class StorageTest {
	
	private List<Map<String,String>> expected;
	private String testString;
	
	@Before
	public void populateDataArray() {
		testString = "key1=value1;key2=value2\nkeyA=valueA\nkeyB=valueB;key z=valueB; key !=!@Rdfbte";
		expected = new ArrayList<>();
        Map<String, String> tempMap = new HashMap<>();
        tempMap.put("key1", "value1");
        tempMap.put("key2", "value2");
        expected.add(tempMap);
        
        tempMap = new HashMap<>();
        tempMap.put("keyA", "valueA");
        expected.add(tempMap);
        
        tempMap = new HashMap<>();
        tempMap.put("keyB", "valueB");
        tempMap.put("key z", "valueB");
        tempMap.put(" key !", "!@Rdfbte");
        expected.add(tempMap);
	}
	
    @Test
    public void testStore() {
        Storage classUnderTest = new Storage();
        String actual = classUnderTest.store(expected);
        assertEquals(testString, actual);

    }
    
    @Test
    public void testLoad() {
        Storage classUnderTest = new Storage();
        List<Map<String,String>> actual = classUnderTest.load(testString);
        assertEquals(expected, actual);
    }
}
